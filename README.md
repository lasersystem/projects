# [REDLab](http://redlab.fr/) - Invetaire des projets réalisés et produits 

Ne sont recencés ici que les modèles actuellement produits par les fablabs du réseau REDLab (Occitanie). Ce repository n'a pas vocation à recensé l'ensemble des initiative exitantes.

Avec le soutien de nos partenaires, nos labs se coordonnent pour aider tous ceux qui ont besoins, en produisant bénévolement, du matériel de protection.

![rajaa](faceshields/rajaa.jpg)
![rajaa](faceshields/notice.jpg)
![prod by lahcen](faceshields/bilbao/prod.jpeg)

## Autres repos utiles

* (https://gitlab.com/MonsieurBidouille)[Entraide Maker - COVID19](https://gitlab.com/entraide-maker-covid-19) - Coordonné par notre ami [@MonsieurBidouille]

* [Global fablabs repo](https://gitlab.fabcloud.org/pub/project/coronavirus/tracking)

* MIT Fabfoundation projects [repository](https://gitlab.cba.mit.edu/pub/coronavirus/tracking)

* [Fablab ULB](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields)

## Liens utiles

**attestations:**

Explications sur le [site de la RRFLabs](http://www.fablab.fr/coronavirus/prototypage-et-projets/article/acces-aux-fablabs-en-periode-de-confinement)
* [Attestation de sortie dérogatoire](admin/attestation-deplacement-fr.pdf)
* [Attestation de déplacement professionel](admin/justificatif-deplacement-professionnel-fr.pdf)

## Equipements de protection

### Visières

[![albilab](faceshields/albilab/albilab.jpg)](faceshields.md)

[Voir les projets de visières produits localement](faceshields.md)

### Masques en tissus

[![tuto](masks/labobine.png)](masks.md)
 
[Voir les initiatives de masques en tissus produits localement](masks.md)
 
## Equipements hospitaliers

### Respiratoires

[![charlotte](decathlon_mask/charlotte.png)](faceshields.md)

[Voir les autres projets pour équipements respiratoires](faceshields.md)


